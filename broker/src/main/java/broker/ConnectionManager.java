package broker;

import com.Activity;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.util.SerializationUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ConnectionManager {
    private Channel channel;
    private String exchangeName = "activityExchange";
    private String queueName = "activityQueue";
    private String routingKey = "routingKey";


    public ConnectionManager(){

        ConnectionFactory factory = new ConnectionFactory();
        //System.out.println("1");
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setVirtualHost("/");
        factory.setHost("localhost");
        factory.setPort(5672);

        Connection conn = null;
        try {
            conn = factory.newConnection();
            channel = conn.createChannel();
           // System.out.println("2");



            channel.exchangeDeclare(exchangeName, "direct", true);

            channel.queueDeclare(queueName, true, false, false, null);
            channel.queueBind(queueName, exchangeName, routingKey);

        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

    }

    public void sendActivity (Activity activity){
        byte[] messageBodyBytes = SerializationUtils.serialize(activity);
        try {
            channel.basicPublish(exchangeName, routingKey, null, messageBodyBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
