package broker;

import com.Activity;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.SerializationUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


@RestController
@CrossOrigin
public class ActivityController {

    private ConnectionManager connectionManager;


    public ActivityController(){

        connectionManager = new ConnectionManager();
    }

    @RequestMapping("/activity")
    public void getActivity(@RequestBody Activity activity){
        System.out.println(activity.toString());
        connectionManager.sendActivity(activity);

    }


    @RequestMapping("/")
    public void test (){
        System.out.println("test");
    }


}
