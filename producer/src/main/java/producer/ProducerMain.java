package producer;

import com.Activity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import com.sun.deploy.net.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.*;
import java.util.Date;
import java.util.List;

import sun.net.www.http.HttpClient;

import static java.lang.Thread.sleep;

public class ProducerMain {

    public static void main(String[] args) {
//        RestTemplate restTemplate = new RestTemplate();
//        final String baseUrl = "http://localhost:8080/activity/";
//        try {
//            URI uri = new URI(baseUrl);
//            Activity activity = new Activity(
//                    875,
//                    new Date(98432232),
//                    new Date(234),
//                    "name of activity" );
//            ResponseEntity<String> result = restTemplate.postForEntity(uri,activity,String.class);
//            System.out.println("should be 201:  " + result.getStatusCode());
//
//
//
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }


        try {

            ActivityReader activityReader = new ActivityReader();
            List<Activity> activityList = activityReader.getActivities();


            // Creating Object of ObjectMapper define in Jakson Api
            ObjectMapper objectMapper = new ObjectMapper();


            //System.out.println(jsonInputString);




            for(Activity activity : activityList ){
                String jsonInputString = objectMapper.writeValueAsString(activity);
                //System.out.println(jsonInputString);
                System.out.println(activity.toString());


                URL url = new URL ("http://localhost:8080/activity");
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json; utf-8");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);


                try(OutputStream os = con.getOutputStream()) {
                    byte[] input = jsonInputString.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    System.out.println(response.toString());
                }
                con.disconnect();

                sleep(100);

            }








        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }


//        JSONObject jsonObject;
//
//
//        String       postUrl       = "www.site.com";// put in your url
//        Gson         gson          = new Gson();
//        HttpClient httpClient    = HttpClientBuilder.create().build();
//        HttpPost post          = new HttpPost(postUrl);
//        StringEntity postingString = new StringEntity(gson.toJson(pojo1));//gson.tojson() converts your pojo to json
//        post.setEntity(postingString);
//        post.setHeader("Content-type", "application/json");
//        HttpResponse response = httpClient.execute(post);

    }


//    public static void main2(String[] args) {
//        System.out.println("0");
//        ConnectionFactory factory = new ConnectionFactory();
//
//
//        try {
//
//            System.out.println("1");
//            factory.setUsername("guest");
//            factory.setPassword("guest");
//            factory.setVirtualHost("/");
//            factory.setHost("localhost");
//            factory.setPort(5672);
//
//            Connection conn = factory.newConnection();
//            Channel channel = conn.createChannel();
//            System.out.println("2");
//
//            String exchangeName = "activityExchange";
//            String queueName = "activityQueue";
//            String routingKey = "routingKey";
//
//            channel.exchangeDeclare(exchangeName, "direct", true);
//
//            channel.queueDeclare(queueName, true, false, false, null);
//            channel.queueBind(queueName, exchangeName, routingKey);
//
//
//            byte[] messageBodyBytes = "Hello, world!".getBytes();
//            channel.basicPublish(exchangeName, routingKey, null, messageBodyBytes);
//
//
//
//
//            channel.close();
//            conn.close();
//            System.out.println("4");
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (TimeoutException e) {
//            e.printStackTrace();
//        }
//    }
}