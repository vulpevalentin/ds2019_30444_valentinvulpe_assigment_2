package producer;

import com.Activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ActivityReader {
    private final Integer patientID = 892;

    public List<Activity> getActivities(){
        List<Activity> activityList = new ArrayList<>();

        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("producer/src/main/resources/activity.txt"));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();


                // process the line
                Activity activity = this.processLine(line);

                //System.out.println(activity);
                activityList.add(activity);

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }




        return activityList;
    }
    private Activity processLine(String line){
        Activity activity = new Activity();
        activity.setPatientID(patientID);


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        StringTokenizer stringTokenizer;
        stringTokenizer = new StringTokenizer(line, "\t");
        try {
            //System.out.println(line);
            String timeStart = stringTokenizer.nextToken();
            activity.setTimeStart(dateFormat.parse(timeStart));
            //System.out.println("time start:  "+ timeStart+"      " + dateFormat.format(activity.getTimeStart()));
            String timeEnd = stringTokenizer.nextToken();
            activity.setTimeEnd(dateFormat.parse(timeEnd));
            String name = stringTokenizer.nextToken();
            activity.setActivityName(name);

           // System.out.println(activity.toString());
           //System.out.println();


        } catch (ParseException e) {
            e.printStackTrace();
        }





        return activity;
    }
}
