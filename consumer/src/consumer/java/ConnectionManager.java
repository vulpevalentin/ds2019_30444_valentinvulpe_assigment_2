import com.Activity;
import com.rabbitmq.client.*;
import org.springframework.util.SerializationUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ConnectionManager {


    public ConnectionManager() {}

    public void startConsumer(){
        ConnectionFactory factory = new ConnectionFactory();

        String queueName = "activityQueue";


//        System.out.println("1");
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setVirtualHost("/");
        factory.setHost("localhost");
        factory.setPort(5672);


        try {
            final Connection conn = factory.newConnection();
            final Channel channel = conn.createChannel();


            final boolean autoAck = false;
            channel.basicConsume(queueName, autoAck, "myConsumerTag",
                    new DefaultConsumer(channel) {
                        @Override
                        public void handleDelivery(String consumerTag,
                                                   Envelope envelope,
                                                   AMQP.BasicProperties properties,
                                                   byte[] body)
                                throws IOException {
                            String routingKey = envelope.getRoutingKey();
                            String contentType = properties.getContentType();
                            long deliveryTag = envelope.getDeliveryTag();
                            // (process the message components here ...)

                            Activity activity = (Activity) SerializationUtils.deserialize(body);
                            ActivityRules.checkRules(activity);

                            System.out.println(activity.toString());

                            channel.basicAck(deliveryTag, false);

                        }
                    });


        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }


}

