import com.Activity;

public class ActivityRules {

    private static long hour = 1000 * 60 * 60;
    private static long halfday = hour * 12;

    private static boolean rule1(Activity activity){
        if(activity.getActivityName().equals("Sleeping"))
        {
            if(activity.getTimeEnd().getTime() - activity.getTimeStart().getTime() > halfday) {

                System.out.println("\nRule 1 Violated: Sleep longer than 12 hours!");
                System.out.println(activity.toString() + "\n");
                return true;
            }
        }
        return false;
    }
    private static boolean rule2(Activity activity){
        if(activity.getActivityName().equals("Leaving"))
        {
            if(activity.getTimeEnd().getTime() - activity.getTimeStart().getTime() > halfday){
                System.out.println("\nRule 2 Violated: Leaving longer than 12 hours!");
                System.out.println(activity.toString() + "\n");
                return true;
            }

        }
        return false;
    }
    private static boolean rule3(Activity activity){
        if(
                activity.getActivityName().equals("Toileting") ||
                activity.getActivityName().equals("Showering") ||
                activity.getActivityName().equals("Grooming"))
        {
            if(activity.getTimeEnd().getTime() - activity.getTimeStart().getTime() > hour){
                System.out.println("\nRule 3 Violated: Time in bathroom longer than 1 hour!");
                System.out.println(activity.toString() + "\n");
                return true;
            }

        }
        return false;
    }
    public static boolean checkRules(Activity activity){

        return rule1(activity) || rule2(activity) || rule3(activity);
    }
}
